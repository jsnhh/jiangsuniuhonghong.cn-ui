import request from '@/utils/request'

// 数据概率
export function indexOverviewData(query) {
    return request({
        url: '/api/user/indexDataOverview',
        method: 'get',
        params: query
    })
}
// 交易数据
export function indexTransactionData(query) {
    return request({
        url: '/api/user/indexTransactionData',
        method: 'post',
        params: query
    })
}

// 支付通道
export function passagewayStatistics(query) {
    return request({
        url: '/api/user/passagewayStatistics',
        method: 'post',
        params: query
    })
}