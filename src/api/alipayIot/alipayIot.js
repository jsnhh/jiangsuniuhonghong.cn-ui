import request from '@/utils/request'
// 代运营授权列表
export function operationList(query) {
    return request({
        url: '/api/alipayopen/operationList',
        method: 'post',
        params: query
    })
}

// 查询授权状态
export function iotOperationBindQuery(query) {
    return request({
        url: '/api/alipayopen/iotOperationBindQuery',
        method: 'post',
        params: query
    })
}

// 新蓝海列表
export function getIotBlueseaList(query) {
    return request({
        url: '/api/alipayopen/iotBlueseaList',
        method: 'post',
        params: query
    })
}

// 查询报名结果
export function iotBluesQuery(query) {
    return request({
        url: '/api/alipayopen/iotBluesQuery',
        method: 'post',
        params: query
    })
}

// 代运营授权
export function iotOperationBind(query) {
    return request({
        url: '/api/alipayopen/iotOperationBind',
        method: 'post',
        params: query
    })
}

// 绑定门店
export function iotOperationStoreQuery(query) {
    return request({
        url: '/api/alipayopen/iotOperationStoreQuery',
        method: 'post',
        params: query
    })
}

// 提交绑定
export function iotBindDevice(query) {
    return request({
        url: '/api/alipayopen/iotBindDevice',
        method: 'post',
        params: query
    })
}

// 获取商铺
export function shopType(query) {
    return request({
        url: '/api/alipayopen/shopType',
        method: 'get',
        params: query
    })
}

// 保存图片
export function upStore(query) {
    return request({
        url: '/api/user/up_store',
        method: 'post',
        params: query
    })
}

//新蓝海报名 提交
export function iotBlueseaCreate(query) {
    return request({
        url: '/api/alipayopen/iotBlueseaCreate',
        method: 'post',
        params: query
    })
}

//新蓝海报名 详情
export function blueseaInfo(query) {
    return request({
        url: '/api/alipayopen/blueseaInfo',
        method: 'post',
        params: query
    })
}

//新蓝海报名 修改
export function iotBlueseaUp(query) {
    return request({
        url: '/api/alipayopen/iotBlueseaUp',
        method: 'post',
        params: query
    })
}