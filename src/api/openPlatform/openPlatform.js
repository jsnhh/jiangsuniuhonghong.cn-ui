import request from '@/utils/request'

// 支付宝开放平台配置
export function aliThirdConfigInfo(data) {
    return request({
        url: '/api/user/ali_third_config_info',
        method: 'post',
        data: data
    })
}
// 微信开放平台配置
export function wxThirdConfigInfo(data) {
    return request({
        url: '/api/user/wechat_third_config_info',
        method: 'post',
        data: data
    })
}
// 保存微信开放平台配置
export function saveWechatThirdConfig(data) {
    return request({
        url: '/api/user/save_wechat_third_config',
        method: 'post',
        data: data
    })
}
// 保存微信开放平台配置
export function saveAliThirdConfig(data) {
    return request({
        url: '/api/user/save_ali_third_config',
        method: 'post',
        data: data
    })
}