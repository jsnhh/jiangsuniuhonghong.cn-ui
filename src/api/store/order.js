import request from '@/utils/request'

// 订单列表
export function orderList(data) {
    return request({
      url: '/api/user/order',
      method: 'post',
      data: data
    })
}
  
