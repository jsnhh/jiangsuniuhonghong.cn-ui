import request from '@/utils/request'

// 查询
export function listDevice(data) {
    return request({
        url: '/api/device/lists',
        method: 'post',
        data: data
    })
}
//详情
export function listDeviceDetail(data) {
    return request({
        url: '/api/device/select',
        method: 'post',
        data: data
    })
}

// 新增
export function addDevice(data) {
    return request({
        url: '/api/device/add',
        method: 'post',
        data: data
    })
}

// 更新
export function updateDevice(data) {
    return request({
        url: '/api/device/up',
        method: 'post',
        data: data
    })
}

// 删除
export function delDevice(data) {
    return request({
        url: '/api/device/del',
        method: 'post',
        data: data
    })
}
//选择员工
export function selectMerchant(data) {
    return request({
        url: '/api/basequery/merchant_lists',
        method: 'post',
        data: data
    })
}
//选择设备类型
export function selectDeviceType(data) {
    return request({
        url: '/api/device/device_type',
        method: 'post',
        data: data
    })
}



//系统管理中的设备列表
export function deviceOemLists(data) {
    return request({
        url: '/api/device/device_oem_lists',
        method: 'post',
        data: data
    })
}
//系统管理中的添加设备
export function deviceOemAdd(data) {
    return request({
        url: '/api/device/device_oem_add',
        method: 'post',
        data: data
    })
}
//系统管理中的添加设备
export function deviceOemUp(data) {
    return request({
        url: '/api/device/device_oem_up',
        method: 'post',
        data: data
    })
}
//系统管理中的设备删除
export function deviceOemDel(data) {
    return request({
        url: '/api/device/device_oem_del',
        method: 'post',
        data: data
    })
}

//设备配置
export function deviceConfig(data) {
    return request({
        url: '/api/device/v_config',
        method: 'post',
        data: data
    })
}