import request from '@/utils/request'

//查询通道管理
export function listPayWays(data) {
  return request({
    url: '/api/user/pay_ways_all',
    method: 'post',
    data: data
  })
}
//查询单个通道详细
export function payWaysInfoDetail(data) {
  return request({
    url: '/api/user/pay_ways_info',
    method: 'post',
    data: data
  })
}

// 清除通道
export function clearPayWays(data) {
  return request({
    url: '/api/user/clear_ways_type',
    method: 'post',
    data: data
  })
}

// 通道禁用查询
export function querySetPayWays(data) {
  return request({
    url: '/api/user/store_ways_select',
    method: 'post',
    data: data
  })
}
// 通道禁用
export function setPayWays(data) {
  return request({
    url: '/api/user/store_ways_set',
    method: 'post',
    data: data
  })
}
// pc端开通通道
export function openWaysType(data) {
  return request({
    url: '/api/user/open_ways_type',
    method: 'post',
    data: data
  })
}

// 更新门店设置费率-扫码
export function updatePayWays(data) {
  return request({
    url: '/api/user/edit_store_rate',
    method: 'post',
    data: data
  })
}

// 商户功能变更(易生)
export function alterFunc(data) {
  return request({
    url: '/api/user/alterFunc',
    method: 'post',
    data: data
  })
}

// 开通通道-支付宝授权
export function alipayAuth(data) {
  return request({
    url: '/api/user/alipay_auth',
    method: 'post',
    data: data
  })
}

//结算类型查询
export function settleModeType(query) {
  return request({
    url: '/api/basequery/settle_mode_type',
    method: 'get',
    params: query
  })
}

//申请通道
export function openWays(data) {
  return request({
    url: '/api/basequery/openways',
    method: 'post',
    data: data
  })
}

//微信实名认证
export function applyWeChatRealName(data) {
  return request({
    url: '/api/user/applyWeChatRealName',
    method: 'post',
    data: data
  })
}
//微信实名认证 结果查询
export function queryWeChatRealName(data) {
  return request({
    url: '/api/user/queryWeChatRealName',
    method: 'post',
    data: data
  })
}
//微信子商户授权状态 查询
export function queryGrantStatusWeChatRealName(data) {
  return request({
    url: '/api/user/queryGrantStatusWeChatRealName',
    method: 'post',
    data: data
  })
}
//微信实名认证 申请撤销
export function backWeChatRealName(data) {
  return request({
    url: '/api/user/backWeChatRealName',
    method: 'post',
    data: data
  })
}

//查询扣款顺序列表
export function pay_ways_sort(data) {
  return request({
    url: '/api/user/pay_ways_sort',
    method: 'post',
    data: data
  })
}

//扣款顺序初始化
export function pay_ways_sort_start(data) {
  return request({
    url: '/api/user/pay_ways_sort_start',
    method: 'post',
    data: data
  })
}

//编辑扣款顺序
export function pay_ways_sort_edit(data) {
  return request({
    url: '/api/user/pay_ways_sort_edit',
    method: 'post',
    data: data
  })
}

//电子协议-获取第几步的状态
export function queryContract (data) {
  return request({
    url: '/api/user/queryContract',
    method: 'post',
    data: data
  })
}

//电子协议-开户
export function easypayElectronic (data) {
  return request({
    url: '/api/user/easypayElectronic',
    method: 'post',
    data: data
  })
}

//电子协议-填写合同信息
export function addContractInfo (data) {
  return request({
    url: '/api/user/addContractInfo',
    method: 'post',
    data: data
  })
}

//电子协议-发送短信
export function sendSms (data) {
  return request({
    url: '/api/user/sendSms',
    method: 'post',
    data: data
  })
}
//电子协议-创建合同
export function createContract (data) {
  return request({
    url: '/api/user/createContract',
    method: 'post',
    data: data
  })
}
//电子协议-签署合同
export function signContract (data) {
  return request({
    url: '/api/user/signContract',
    method: 'post',
    data: data
  })
}
//电子协议-下载电子协议
export function downloadContract (data) {
  return request({
    url: '/api/user/downloadContract',
    method: 'post',
    data: data
  })
}

//电子协议-数据回显
export function getContract (data) {
  return request({
    url: '/api/user/getContract',
    method: 'post',
    data: data
  })
}

//电子协议-删除
export function deleteContract (data) {
  return request({
    url: '/api/user/deleteContract',
    method: 'post',
    data: data
  })
}
//拉卡拉基本信息修改
export function lklPayUpdateMerchant(data) {
  return request({
      url: '/api/lklpay/updateMerchant',
      method: 'post',
      data: data
  })
}
//拉卡拉基本结算账号信息修改
export function lklPayUpdateMerchantSettle(data) {
  return request({
      url: '/api/lklpay/updateMerchantSettle',
      method: 'post',
      data: data
  })
}
//拉卡拉基本费率信息修改
export function lklPayUpdateMerchantFee(data) {
  return request({
      url: '/api/lklpay/updateMerchantFee',
      method: 'post',
      data: data
  })
}