import request from '@/utils/request'

// 商户入网补充资料
export function ysepayMerchantAccess(data) {
    return request({
        url: '/api/ysepay/ysepayMerchantAccess',
        method: 'post',
        data: data
    })
}
// mcc查询
export function queryMccList(data) {
    return request({
        url: '/api/ysepay/queryMccList',
        method: 'post',
        data: data
    })
}
//入网申请
export function addCustInfoApply(data) {
    return request({
        url: '/api/ysepay/addCustInfoApply',
        method: 'post',
        data: data
    })
}
//商户入网审核
export function auditCustInfoApply(data) {
    return request({
        url: '/api/ysepay/auditCustInfoApply',
        method: 'post',
        data: data
    })
}

//商户基本信息变更申请
export function changeMercBaseInfo(data) {
    return request({
        url: '/api/ysepay/changeMercBaseInfo',
        method: 'post',
        data: data
    })
}
//基本信息变更审核
export function changeBaseAudit(data) {
    return request({
        url: '/api/ysepay/changeBaseAudit',
        method: 'post',
        data: data
    })
}
//结算信息变更申请
export function changeMercStlAccInfo(data) {
    return request({
        url: '/api/ysepay/changeMercStlAccInfo',
        method: 'post',
        data: data
    })
}
//费率信息变更申请
export function changeRate(data) {
    return request({
        url: '/api/ysepay/changeRate',
        method: 'post',
        data: data
    })
}

//合同签约
export function smscSign(data) {
    return request({
        url: '/api/ysepay/smscSign',
        method: 'post',
        data: data
    })
}

//重发签约短信或邮件
export function sendSmsOrEmailMsg(data) {
    return request({
        url: '/api/ysepay/sendSmsOrEmailMsg',
        method: 'post',
        data: data
    })
}

//电子合同查询签约状态
export function queryContract(data) {
    return request({
        url: '/api/ysepay/queryContract',
        method: 'post',
        data: data
    })
}

//电子合同下载
export function downloadContract(data) {
    return request({
        url: '/api/ysepay/downloadContract',
        method: 'post',
        data: data
    })
}

//开通/关闭线上D0权限
export function onlineOpen(data) {
    return request({
        url: '/api/ysepay/onlineOpen',
        method: 'post',
        data: data
    })
}

