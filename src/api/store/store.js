import request from '@/utils/request'

// 查询门店列表
export function listStore(query) {
    return request({
        url: '/api/user/store_pc_lists',
        method: 'get',
        params: query
    })
}

// 门店信息查看
export function storeDetail(data) {
    return request({
        url: '/api/user/store',
        method: 'post',
        data: data
    })
}
// 门店信息-商户功能查看
export function storeBaseInfo(data) {
    return request({
        url: '/api/user/get_store_base_info',
        method: 'post',
        data: data
    })
}


// 新增门店信息
export function addStore(data) {
    return request({
        url: '/api/user/up_store',
        method: 'post',
        data: data
    })
}

// 新增分店门店信息
export function addSubStore(data) {
    return request({
        url: '/api/user/add_sub_store',
        method: 'post',
        data: data
    })
}

// 更新门店信息
export function updateStore(data) {
    return request({
        url: '/api/user/up_store',
        method: 'post',
        data: data
    })
}

// 删除门店信息
export function delStore(data) {
    return request({
        url: '/api/user/del_store',
        method: 'post',
        data: data
    })
}

// 恢复门店信息
export function recStore(data) {
    return request({
        url: '/api/user/rec_store',
        method: 'post',
        data: data
    })
}

// 清除门店
export function clearStore(data) {
    return request({
        url: '/api/user/clear_store',
        method: 'post',
        data: data
    })
}

// 关闭门店
export function closeStore(data) {
    return request({
        url: '/api/user/col_store',
        method: 'post',
        data: data
    })
}

// 开启门店
export function openStore(data) {
    return request({
        url: '/api/user/ope_store',
        method: 'post',
        data: data
    })
}

// 门店转移
export function changeStore(data) {
    return request({
        url: '/api/user/update_user',
        method: 'post',
        data: data
    })
}

// 微收银插件API
export function qwxCodeList(data) {
    return request({
        url: '/api/qwx/code_list',
        method: 'post',
        data: data
    })
}
// 新增微收银插件
export function addQwxCode(data) {
    return request({
        url: '/api/qwx/add_code',
        method: 'post',
        data: data
    })
}
// 删除微收银插件
export function deleteQwxCode(data) {
    return request({
        url: '/api/qwx/del_code',
        method: 'post',
        data: data
    })
}

//收钱啦插件API
export function winCodeList(data) {
    return request({
        url: '/api/wincode/code_list',
        method: 'post',
        data: data
    })
}
//新增收钱啦插件
export function addWinCode(data) {
    return request({
        url: '/api/wincode/add_code',
        method: 'post',
        data: data
    })
}
//删除收钱啦插件
export function deleteWinCode(data) {
    return request({
        url: '/api/wincode/del_code',
        method: 'post',
        data: data
    })
}
//达达配送
export function updateDaDaConfig(data) {
    return request({
        url: '/api/user/updateDaDaConfig',
        method: 'post',
        data: data
    })
}
//完善小程序信息
export function getStoreAppletsInfo(data) {
    return request({
        url: '/api/customer/store/getStoreAppletsInfo',
        method: 'post',
        data: data
    })
}
//根据支付宝小程序名称搜索小程序的appid
export function getAppletAppId(data) {
    return request({
        url: '/api/customer/aliPay/getAppletAppId',
        method: 'post',
        data: data
    })
}

//新大陆D0列表
export function newlandList(data) {
    return request({
        url: '/api/newland/store_list',
        method: 'post',
        data: data
    })
}
//开启新大陆D0
export function openNewland(data) {
    return request({
        url: '/api/newland/open_da',
        method: 'post',
        data: data
    })
}

export function newlandInfo(data) {
    return request({
        url: '/api/newland/get_da_info',
        method: 'post',
        data: data
    })
}
//收银员列表
export function cashierList(data) {
    return request({
        url: '/api/user/merchant_lists',
        method: 'post',
        data: data
    })
}
//收银员详情
export function cashierDetail(data) {
    return request({
        url: '/api/user/merchant_info',
        method: 'post',
        data: data
    })
}
//新增收银员
export function addCashier(data) {
    return request({
        url: '/api/user/add_merchant',
        method: 'post',
        data: data
    })
}
//修改收银员
export function updateCashier(data) {
    return request({
        url: '/api/user/up_merchant',
        method: 'post',
        data: data
    })
}
//删除收银员
export function deleteCashier(data) {
    return request({
        url: '/api/user/del_merchant',
        method: 'post',
        data: data
    })
}

//门店收款码
export function storePayQr(data) {
    return request({
        url: '/api/user/store_pay_qr',
        method: 'post',
        data: data
    })
}
//获取门店openid二维码
export function storeOpenidQr(data) {
    return request({
        url: '/api/user/store_openid_qr',
        method: 'post',
        data: data
    })
}
//审核门店
export function checkStore(data) {
    return request({
        url: '/api/user/check_store',
        method: 'post',
        data: data
    })
}

//同步资料(商户信息变更)
export function alterMerchant(data) {
    return request({
        url: '/api/user/alterMerchant',
        method: 'post',
        data: data
    })
}
//结算账户变更，该接口用于变更结算账户信息
export function alterPayAcc(data) {
    return request({
        url: '/api/user/alterPayAcc',
        method: 'post',
        data: data
    })
}

//项目归属
export function getProjectInfo(data) {
    return request({
        url: '/api/user/getProjectId',
        method: 'post',
        data: data
    })
}
//获取区号
export function getAreaNo(data) {
    return request({
        url: '/api/user/getAreaNo',
        method: 'post',
        data: data
    })
}
//获取mcc，门店分类
export function getMcc(data) {
    return request({
        url: '/api/user/getMcc',
        method: 'post',
        data: data
    })
}
//获取银行信息 1-总行；2-支行
export function getBanks(data) {
    return request({
        url: '/api/user/getBanks',
        method: 'post',
        data: data
    })
}
//获取商户信息
export function easyPayMerchantAccess(data) {
    return request({
        url: '/api/user/easyPayMerchantAccess',
        method: 'post',
        data: data
    })
}
//修改商户零费率状态
export function updateZeroRateType(data) {
    return request({
        url: '/api/user/edit_store_zero_rate',
        method: 'post',
        data: data
    })
}

// 获取门店
export function getStoreLists(query) {
    return request({
        url: '/api/user/store_lists',
        method: 'post',
        params: query
    })
}

// 获取商户充值信息
export function merchant_payments_list(data) {
    return request({
        url: '/api/user/merchant_payments_list',
        method: 'post',
        data: data
    })
}

// 审核易生进件资料
export function checkEasypayStore(data) {
    return request({
        url: '/api/user/checkEasypayStore',
        method: 'post',
        data: data
    })
}

// 创建商户号
export function createMerchantNo(data) {
    return request({
        url: '/api/user/createMerchantNo',
        method: 'post',
        data: data
    })
}
//获取区号 拉卡拉
export function getLklAreaNo(data) {
    return request({
        url: '/api/lklpay/queryAreaList',
        method: 'post',
        data: data
    })
}
//获取银行区号 拉卡拉
export function getLklBankAreaNo(data) {
    return request({
        url: '/api/lklpay/queryBankAreaList',
        method: 'post',
        data: data
    })
}
//获取mcc，门店分类拉卡拉
export function getLklMcc(data) {
    return request({
        url: '/api/lklpay/queryMccList',
        method: 'post',
        data: data
    })
}
//获取银行信息 拉卡拉
export function getLklBanks(data) {
    return request({
        url: '/api/lklpay/queryBankList',
        method: 'post',
        data: data
    })
}
//获取拉卡拉商户补充信息
export function lklPayMerchantReplenish(data) {
    return request({
        url: '/api/lklpay/lklPayMerchantReplenish',
        method: 'post',
        data: data
    })
}
// 重置商户密码
export function resetStorePwd(data) {
    return request({
        url: '/api/user/reset_store_pwd',
        method: 'post',
        data: data
    })
}