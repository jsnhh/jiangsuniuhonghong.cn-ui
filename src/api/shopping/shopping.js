import request from '@/utils/request'
//查询代理商收货地址
export function getAddressList(query) {
    return request({
        url: '/api/user/getAddressList',
        method: 'post',
        params: query
    })
}
//查询商品类别
export function getShoppingCategoryList(query) {
    return request({
        url: '/api/user/getShoppingCategoryList',
        method: 'post',
        params: query
    })
}
//查询商品类别 下拉选使用
export function shoppingCategorySelect(query) {
    return request({
        url: '/api/user/shoppingCategorySelect',
        method: 'post',
        params: query
    })
}
//新增商品类别
export function addShoppingCategory(query) {
    return request({
        url: '/api/user/addShoppingCategory',
        method: 'post',
        params: query
    })
}
//删除商品
export function delShoppingCategory(query) {
    return request({
        url: '/api/user/delShoppingCategory',
        method: 'post',
        params: query
    })
}
//获取商品列表
export function getShoppingGoodsList(query) {
    return request({
        url: '/api/user/getShoppingGoodsList',
        method: 'post',
        params: query
    })
}
//新增商品
export function addShoppingGoods(query) {
    return request({
        url: '/api/user/addShoppingGoods',
        method: 'post',
        params: query
    })
}
//删除商品
export function delShoppingGoods(query) {
    return request({
        url: '/api/user/delShoppingGoods',
        method: 'post',
        params: query
    })
}
//查询订单列表
export function getShoppingOrderList(query) {
    return request({
        url: '/api/user/getShoppingOrderList',
        method: 'post',
        params: query
    })
}
//发货
export function deliverGoods(query) {
    return request({
        url: '/api/user/deliverGoods',
        method: 'post',
        params: query
    })
}
//订单修改
export function editOrder(query) {
    return request({
        url: '/api/user/editOrder',
        method: 'post',
        params: query
    })
}
//删除订单
export function delOrder(query) {
    return request({
        url: '/api/user/delOrder',
        method: 'post',
        params: query
    })
}
//查询调拨记录
export function getUserTransferList(query) {
    return request({
        url: '/api/user/getUserTransferList',
        method: 'post',
        params: query
    })
}