import request from '@/utils/request'

// 广告列表
export function adLists(query) {
    return request({
        url: '/api/ad/ad_lists',
        method: 'post',
        params: query
    })
}
//广告投放位置
export function adPid(query) {
    return request({
        url: '/api/ad/ad_p_id',
        method: 'post',
        params: query
    })
}
//新增广告
export function adCreate(query) {
    return request({
        url: '/api/ad/ad_create',
        method: 'post',
        params: query
    })
}
//修改广告
export function adUp(query) {
    return request({
        url: '/api/ad/ad_up',
        method: 'post',
        params: query
    })
}
//删除广告
export function adDel(query) {
    return request({
        url: '/api/ad/ad_del',
        method: 'post',
        params: query
    })
}
//广告链接
export function adLinksLists(query) {
    return request({
        url: '/api/adlinks/adlinkslists',
        method: 'post',
        params: query
    })
}
//新增广告链接
export function adlinksCreate(query) {
    return request({
        url: '/api/adlinks/adlinksCreate',
        method: 'post',
        params: query
    })
}
//新增广告链接
export function adlinksUp(query) {
    return request({
        url: '/api/adlinks/adlinksUp',
        method: 'post',
        params: query
    })
}
//删除广告链接
export function adlinksDel(query) {
    return request({
        url: '/api/adlinks/adlinksDel',
        method: 'post',
        params: query
    })
}