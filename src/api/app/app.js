import request from '@/utils/request'
//app配置
export function appOemInfo(query) {
    return request({
        url: '/api/basequery/app_oem_info',
        method: 'post',
        params: query
    })
}
//app配置
export function noticeNews(query) {
    return request({
        url: '/api/user/notice_news',
        method: 'post',
        params: query
    })
}
//app消息类型
export function noticeNewsType(query) {
    return request({
        url: '/api/user/notice_news_type',
        method: 'post',
        params: query
    })
}
//新增app消息
export function addNoticeNews(query) {
    return request({
        url: '/api/user/add_notice_news',
        method: 'post',
        params: query
    })
}
//新增app消息
export function delNoticeNews(query) {
    return request({
        url: '/api/user/del_notice_news',
        method: 'post',
        params: query
    })
}
//banners列表
export function banners(query) {
    return request({
        url: '/api/user/banners',
        method: 'post',
        params: query
    })
}
//banners列表
export function bannerType(query) {
    return request({
        url: '/api/user/banner_type',
        method: 'post',
        params: query
    })
}
//删除banners
export function delBanners(query) {
    return request({
        url: '/api/user/del_banners',
        method: 'post',
        params: query
    })
}
//新增banners
export function addBanners(query) {
    return request({
        url: '/api/user/add_banners',
        method: 'post',
        params: query
    })
}
//查询首页功能
export function merchantIndex(query) {
    return request({
        url: '/api/user/merchant_index',
        method: 'post',
        params: query
    })
}
//新增首页功能
export function addMerchantIndex(query) {
    return request({
        url: '/api/user/add_merchant_index',
        method: 'post',
        params: query
    })
}
//新增首页功能
export function delMerchantIndex(query) {
    return request({
        url: '/api/user/del_merchant_index',
        method: 'post',
        params: query
    })
}