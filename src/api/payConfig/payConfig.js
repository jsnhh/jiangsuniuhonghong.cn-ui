import request from '@/utils/request'

// 支付宝isv配置
export function alipayIsvConfig(data) {
    return request({
        url: '/api/user/alipay_isv_config',
        method: 'post',
        data: data
    })
}
// 微信配置
export function weixinConfig(data) {
    return request({
        url: '/api/user/weixin_config',
        method: 'post',
        data: data
    })
}
// 哆啦宝配置
export function dlbConfig(data) {
    return request({
        url: '/api/user/dlb_config',
        method: 'post',
        data: data
    })
}
// 易生支付配置
export function easypayConfig(data) {
    return request({
        url: '/api/user/easypay_config',
        method: 'post',
        data: data
    })
}
// 随行付配置
export function vbillConfig(data) {
    return request({
        url: '/api/user/vbill_config',
        method: 'post',
        data: data
    })
}
// 支付宝如意配置
export function alipaySpiConfig(data) {
    return request({
        url: '/api/aliruyi/alipay_spi_config',
        method: 'post',
        data: data
    })
}