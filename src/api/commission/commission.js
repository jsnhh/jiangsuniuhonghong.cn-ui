import request from '@/utils/request'
// 日分润列表
export function getSettlementDayList(query) {
    return request({
        url: '/api/user/getSettlementDayList',
        method: 'post',
        params: query
    })
}
// 日分润列表
export function getSettlementMonthList(query) {
    return request({
        url: '/api/user/getSettlementMonthList',
        method: 'post',
        params: query
    })
}
// 激活奖励列表
export function getActivationList(query) {
    return request({
        url: '/api/user/getActivationList',
        method: 'post',
        params: query
    })
}
// 交易达标返现
export function getUserReturnAmountList(query) {
    return request({
        url: '/api/user/getUserReturnAmountList',
        method: 'post',
        params: query
    })
}

// 交易达标返现
export function getUserServiceChargeList(query) {
    return request({
        url: '/api/user/getUserServiceChargeList',
        method: 'post',
        params: query
    })
}

// 培养奖列表
export function getUserTrainRewardList(query) {
    return request({
        url: '/api/user/getUserTrainRewardList',
        method: 'post',
        params: query
    })
}

// 获取赏金来源
export function getSourceType(query) {
    return request({
        url: '/api/wallet/source_type',
        method: 'post',
        params: query
    })
}

// 获取赏金来源
export function getPutForwardList(query) {
    return request({
        url: '/api/wallet/out_wallet_list',
        method: 'post',
        params: query
    })
}

//商家结算
export function settlement(query) {
    return request({
        url: '/api/wallet/settlement',
        method: 'post',
        params: query
    })
}

//服务商结算记录
export function settlementLists(query) {
    return request({
        url: '/api/wallet/settlement_lists',
        method: 'post',
        params: query
    })
}

//服务商结算明细
export function settlementListInfos(query) {
    return request({
        url: '/api/wallet/settlement_list_infos',
        method: 'post',
        params: query
    })
}

//确认结算
export function settlementListTrue(query) {
    return request({
        url: '/api/wallet/settlement_list_true',
        method: 'post',
        params: query
    })
}

//删除结算记录
export function settlementDel(query) {
    return request({
        url: '/api/wallet/settlement_list_del',
        method: 'post',
        params: query
    })
}

//零费率日结算记录
export function zeroSettlementDLists(query) {
    return request({
        url: '/api/wallet/zero_settlement_d_lists',
        method: 'post',
        params: query
    })
}

//零费率月结算记录
export function zeroSettlementMLists(query) {
    return request({
        url: '/api/wallet/zero_settlement_m_lists',
        method: 'post',
        params: query
    })
}

//赏金日结设置列表
export function settlementDayQuery(query) {
    return request({
        url: '/api/wallet/settlementDayQuery',
        method: 'post',
        params: query
    })
}

//赏金日结设置
export function changeDaily(query) {
    return request({
        url: '/api/wallet/changeDaily',
        method: 'post',
        params: query
    })
}

//添加赏金日结设置
export function addSettlement(query) {
    return request({
        url: '/api/wallet/addSettlement',
        method: 'post',
        params: query
    })
}

//修改赏金日结设置
export function upSettlement(query) {
    return request({
        url: '/api/wallet/upSettlement',
        method: 'post',
        params: query
    })
}

//获取提现设置配置
export function settlementConfigs(query) {
    return request({
        url: '/api/wallet/settlement_configs',
        method: 'post',
        params: query
    })
}
//获取代理未提现列表
export function getUserMoney(query) {
    return request({
        url: '/api/wallet/get_user_money',
        method: 'post',
        params: query
    })
}
//更新提现状态
export function editWalletStatus(query) {
    return request({
        url: '/api/wallet/edit_wallet_status',
        method: 'post',
        params: query
    })
}
//下级购买商品奖励列表
export function getUserShoppingRewardListPc(query) {
    return request({
        url: '/api/user/getUserShoppingRewardListPc',
        method: 'post',
        params: query
    })
}
//下级交易返现
export function getSubUserTrainRewardListPc(query) {
    return request({
        url: '/api/user/getSubUserTrainRewardListPc',
        method: 'post',
        params: query
    })
}
//音箱返现
export function getUserBoxRebateList(query) {
    return request({
        url: '/api/user/getUserBoxRebateList',
        method: 'post',
        params: query
    })
}
//架构奖励
export function getUserArchitectureRewardList(query) {
    return request({
        url: '/api/user/getUserArchitectureRewardList',
        method: 'post',
        params: query
    })
}