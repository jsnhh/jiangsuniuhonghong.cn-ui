import request from '@/utils/request'
// 商户空码列表
export function qrLists(query) {
    return request({
        url: '/api/user/QrLists',
        method: 'post',
        params: query
    })
}

// 生成商户空码
export function createQr(query) {
    return request({
        url: '/api/user/createQr',
        method: 'post',
        params: query
    })
}

// 生成商户空码
export function DownloadQr(query) {
    return request({
        url: '/api/user/DownloadQr',
        method: 'post',
        params: query
    })
}

// 已绑定明细
export function qrListInfos(query) {
    return request({
        url: '/api/user/QrListinfos',
        method: 'post',
        params: query
    })
}

// 解绑
export function unbindQr(query) {
    return request({
        url: '/api/user/unbindQr',
        method: 'post',
        params: query
    })
}

//划拨
export function transferCode(query) {
    return request({
        url: '/api/user/transferCode',
        method: 'post',
        params: query
    })
}
//回拨
export function backCode(query) {
    return request({
        url: '/api/user/backCode',
        method: 'post',
        params: query
    })
}