import request from '@/utils/request'
// 查询交易返现规则
export function getRuleList(query) {
    return request({
        url: '/api/user/ruleList',
        method: 'get',
        params: query
    })
}
// 添加交易返现规则
export function addCashBackRule(query) {
    return request({
        url: '/api/user/add_cash_back_rule',
        method: 'post',
        params: query
    })
}
// 修改交易返现规则
export function upCashBackRule(query) {
    return request({
        url: '/api/user/up_cash_back_rule',
        method: 'post',
        params: query
    })
}
// 检测规则
export function testingStandard(query) {
    return request({
        url: '/api/user/testing_standard',
        method: 'post',
        params: query
    })
}
// 规则下拉选
export function selRule(query) {
    return request({
        url: '/api/user/selRule',
        method: 'post',
        params: query
    })
}
// 删除规则
export function delCashBackRule(query) {
    return request({
        url: '/api/user/del_cash_back_rule',
        method: 'post',
        params: query
    })
}
// 查询商户交易达标列表
export function storeTransactionRewardList(query) {
    return request({
        url: '/api/user/store_transaction_reward_list',
        method: 'post',
        params: query
    })
}
// 查询商户交易达标列表
export function getStoreTransactionReward(query) {
    return request({
        url: '/api/user/get_store_transaction_reward',
        method: 'post',
        params: query
    })
}
// 代理商返现列表
export function getAgentRewardList(query) {
    return request({
        url: '/api/user/userRules',
        method: 'get',
        params: query
    })
}
// 支付宝红包
export function getAlipayRedList(query) {
    return request({
        url: '/api/huodong/get_list',
        method: 'get',
        params: query
    })
}
// 添加支付宝红包
export function addAlipayRed(query) {
    return request({
        url: '/api/huodong/add',
        method: 'post',
        params: query
    })
}

// 删除支付宝红包
export function delAlipayRed(query) {
    return request({
        url: '/api/huodong/del',
        method: 'post',
        params: query
    })
}

// 获取门店返还手续费
export function getActivityStoreRateList(query) {
    return request({
        url: '/api/user/activity_store_rate_list',
        method: 'post',
        params: query
    })
}

// 获取门店通道
export function getStorePayWayLists(query) {
    return request({
        url: '/api/user/store_open_pay_way_lists',
        method: 'post',
        params: query
    })
}
// 新增商户返还手续费
export function addActivityStoreRate(query) {
    return request({
        url: '/api/user/add_activity_store_rate',
        method: 'post',
        params: query
    })
}
// 删除商户返还手续费
export function delActivityStoreRate(query) {
    return request({
        url: '/api/user/del_activity_store_rate',
        method: 'post',
        params: query
    })
}