import request from '@/utils/request'

// 查询权限列表
export function listPermission(query) {
  return request({
    url: '/api/role_permission/permission_list',
    method: 'get',
    params: query
  })
}

// 查询权限下拉树结构
export function treeselect() {
  return request({
    url: '/api/role_permission/treeselect',
    method: 'get'
  })
}

// 根据角色ID查询权限下拉树结构
export function roleMenuTreeselect(query) {
  return request({
    url: '/api/role_permission/roleMenuTreeselect',
    method: 'get',
    params: query
  })
}

// 新增权限
export function addPermission(data) {
  return request({
    url: '/api/role_permission/add_permission',
    method: 'post',
    data: data
  })
}

// 修改权限
export function updatePermission(data) {
  return request({
    url: '/api/role_permission/update_permission',
    method: 'post',
    data: data
  })
}

// 删除权限
export function delPermission(data) {
  return request({
    url: '/api/role_permission/del_permission',
    method: 'post',
    data: data
  })
}