import request from '@/utils/request'

// 查询用户列表
export function listUser(query) {
  return request({
    url: '/api/user/get_sub_users',
    method: 'get',
    params: query
  })
}

// 查询用户详细
export function getUser(query) {
  return request({
    url: '/api/user/user_info',
    method: 'get',
    params: query
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/api/user/users_lists_all',
    method: 'post',
    data: data
  })
}

// 更新用户
export function updateUser(data) {
  return request({
    url: '/api/user/users_lists_all',
    method: 'post',
    data: data
  })
}

// 删除用户
export function delUser(data) {
  return request({
    url: '/api/user/users_lists_all',
    method: 'post',
    data: data
  })
}