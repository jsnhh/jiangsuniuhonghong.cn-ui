import request from '@/utils/request'

// logo配置
export function appLogosInfo(query) {
    return request({
        url: '/api/basequery/app_logos_info',
        method: 'post',
        params: query
    })
}
//推送配置
export function pushInfo(query) {
    return request({
        url: '/api/basequery/j_push_info',
        method: 'post',
        params: query
    })
}
//短信类型
export function smsType(query) {
    return request({
        url: '/api/basequery/sms_type',
        method: 'post',
        params: query
    })
}
//获取短信配置
export function smsInfo(query) {
    return request({
        url: '/api/basequery/sms_info',
        method: 'post',
        params: query
    })
}
//mqtt配置
export function mqttConfig(query) {
    return request({
        url: '/api/user/mqtt_config',
        method: 'post',
        params: query
    })
}
//apk配置列表
export function apkLists(query) {
    return request({
        url: '/api/apk/apkLists',
        method: 'post',
        params: query
    })
}
//删除apk
export function apkDel(query) {
    return request({
        url: '/api/apk/apkDel',
        method: 'post',
        params: query
    })
}
//修改apk
export function apkUp(query) {
    return request({
        url: '/api/apk/apkUp',
        method: 'post',
        params: query
    })
}
//新增apk
export function apkCreate(query) {
    return request({
        url: '/api/apk/apkCreate',
        method: 'post',
        params: query
    })
}