import request from '@/utils/request'

// 查询角色列表
export function listRole(query) {
  return request({
    url: '/api/role_permission/role_list',
    method: 'get',
    params: query
  })
}

// 查询角色详细
export function getRole(query) {
  return request({
    url: '/api/role_permission/role_list',
    method: 'get',
    params: query
  })
}

// 新增角色
export function addRole(data) {
  return request({
    url: '/api/role_permission/add_role',
    method: 'post',
    data: data
  })
}

// 更新角色
export function updateRole(data) {
  return request({
    url: '/api/role_permission/update_role',
    method: 'post',
    data: data
  })
}

// 删除角色
export function delRole(data) {
  return request({
    url: '/api/role_permission/del_role',
    method: 'post',
    data: data
  })
}
