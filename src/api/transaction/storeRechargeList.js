import request from '@/utils/request'
// 查询商户充值列表
export function getRechargeList(query) {
    return request({
        url: '/api/user/getRechargeList',
        method: 'post',
        params: query
    })
}