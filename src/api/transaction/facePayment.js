import request from '@/utils/request'
// 刷脸日去重
export function getFacePaymentStatistics(query) {
    return request({
        url: '/api/user/face_payment_statistics',
        method: 'post',
        params: query
    })
}
// 刷脸去重
export function getFacePaymentDis(query) {
    return request({
        url: '/api/user/face_payment_dis',
        method: 'post',
        params: query
    })
}