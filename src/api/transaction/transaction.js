import request from '@/utils/request'
// 查询返佣列表
export function getCommissionList(query) {
    return request({
        url: '/api/wallet/commission_lists',
        method: 'post',
        params: query
    })
}
// 查询商户订单列表
export function getOrderList(query) {
    return request({
        url: '/api/user/order',
        method: 'post',
        params: query
    })
}

//获取通道
export function getPayWayList(query) {
    return request({
        url: '/api/user/store_open_pay_way_lists',
        method: 'post',
        params: query
    })
}
//同步订单状态
export function updateOrder(query) {
    return request({
        url: '/api/basequery/update_order',
        method: 'post',
        params: query
    })
}

//获取花呗账单
export function getFqOrder(query) {
    return request({
        url: '/api/user/fq/order',
        method: 'post',
        params: query
    })
}

//获取门店交易统计
export function ranking(query) {
    return request({
        url: '/api/user/ranking',
        method: 'post',
        params: query
    })
}

//商户订单统计
export function orderCount(query) {
    return request({
        url: '/api/user/order_count',
        method: 'post',
        params: query
    })
}