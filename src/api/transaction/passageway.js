import request from '@/utils/request'
// 查询通道使用记录
export function getPassagewayList(query) {
    return request({
        url: '/api/user/getPassagewayList',
        method: 'post',
        params: query
    })
}