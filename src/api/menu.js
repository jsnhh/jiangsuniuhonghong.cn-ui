import request from '@/utils/request'

// 获取路由
export const getRouters = (token) => {
  return request({
    url: '/api/role_permission/getRouters',
    method: 'get',
    params: { token }
  })
}
