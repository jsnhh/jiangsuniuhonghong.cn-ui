import request from '@/utils/request'

// 选择省份
export function queryProvince(data) {
  return request({
    url: '/api/basequery/city',
    method: 'post',
    data
  })
}

// 商户入驻性质
export function getStoreType(query) {
  return request({
    url: '/api/basequery/store_type',
    method: 'get',
    params: query
  })
}

// 门店分类
export function getStoreCategory(query) {
  return request({
    url: '/api/basequery/vbill_store_category',
    method: 'get',
    params: query
  })
}

// 获取商户Mcc-通用
export function getStoreMccList(query) {
  return request({
      url: '/api/basequery/getStoreMccList',
      method: 'get',
      params: query
  })
}

// 选择所属银行
export function getBankInfo(data) {
  return request({
    url: '/api/basequery/bank',
    method: 'post',
    data: data
  })
}
// 选择所属支行
export function getSubBankInfo(data) {
  return request({
    url: '/api/basequery/sub_bank',
    method: 'post',
    data: data
  })
}
// 图片上传
export function uploadImage(data) {
  return request({
    url: '/api/basequery/webupload?act=image',
    method: 'post',
    data: data
  })
}

// 选择业务员
export function getMerchantLists(data) {
  return request({
    url: '/api/basequery/merchant_lists',
    method: 'post',
    data: data
  })
}
// 选择通道
export function getStorePayWayLists(data) {
  return request({
    url: '/api/user/store_open_pay_way_lists',
    method: 'post',
    data: data
  })
}
// 字典查询
export function selectDictionaries(data) {
  return request({
    url: '/api/basequery/selectDictionaries',
    method: 'post',
    data: data
  })
}
