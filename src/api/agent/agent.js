import request from '@/utils/request'
// 查询代理商列表
export function usersListsAll(query) {
    return request({
        url: '/api/user/users_lists_all',
        method: 'get',
        params: query
    })
}
// 设置提现状态
export function updateWithdraw(token, user_id) {
    const data = {
        token,
        user_id
    }
    return request({
        url: '/api/user/set_withdraw',
        method: 'post',
        params: data
    })
}
// 设置零费率状态
export function updateZeroRate(token, user_id) {
    const data = {
        token,
        user_id
    }
    return request({
        url: '/api/user/set_zeroRate',
        method: 'post',
        params: data
    })
}
// 设置进件状态
export function updateIncoming(token, user_id) {
    const data = {
        token,
        user_id
    }
    return request({
        url: '/api/user/set_incoming',
        method: 'post',
        params: data
    })
}
// 设置绑码状态
export function updateBindQr(token, user_id) {
    const data = {
        token,
        user_id
    }
    return request({
        url: '/api/user/set_bind_qr',
        method: 'post',
        params: data
    })
}
// 修改密码
export function editPwd(data) {
    return request({
        url: '/api/user/edit_pwd',
        method: 'post',
        data: data
    })
}
// 重置密码
export function resetPwd(data) {
    return request({
        url: '/api/user/reset_pwd',
        method: 'post',
        data: data
    })
}
// 添加代理商
export function addSubUser(query) {
    return request({
        url: '/api/user/add_sub_user',
        method: 'post',
        params: query
    })
}
// 修改代理商
export function updateUser(query) {
    return request({
        url: '/api/user/up_user',
        method: 'post',
        params: query
    })
}
// 删除代理商
export function delSubUser(query) {
    return request({
        url: '/api/user/del_sub_user',
        method: 'post',
        params: query
    })
}
// 获取代理商信息
export function getUserInfo(query) {
    return request({
        url: '/api/user/user_info',
        method: 'post',
        params: query
    })
}
// 获取角色
export function getRoleList(query) {
    return request({
        url: '/api/role_permission/role_list',
        method: 'post',
        params: query
    })
}
// 获取代理角色
export function getUserRoleList(query) {
    return request({
        url: '/api/role_permission/user_role_list',
        method: 'post',
        params: query
    })
}
// 分配角色
export function assignRole(query) {
    return request({
        url: '/api/role_permission/assign_role',
        method: 'post',
        params: query
    })
}
// 获取用户通道
export function userWaysAll(query) {
    return request({
        url: '/api/user/user_ways_all',
        method: 'post',
        params: query
    })
}
// 修改费率1
export function editUserRate(query) {
    return request({
        url: '/api/user/edit_user_rate',
        method: 'post',
        params: query
    })
}
// 修改费率2
export function editUserUnRate(query) {
    return request({
        url: '/api/user/edit_user_un_rate',
        method: 'post',
        params: query
    })
}
// 修改费率3
export function editUserUnqrRate(query) {
    return request({
        url: '/api/user/edit_user_unqr_rate',
        method: 'post',
        params: query
    })
}
// 设置通道
export function switchUserRate(query) {
    return request({
        url: '/api/user/del_user_rate',
        method: 'post',
        params: query
    })
}

// 获取代理商户默认费率
export function userWaysDefault(query) {
    return request({
        url: '/api/user/user_ways_default',
        method: 'post',
        params: query
    })
}
// 修改商户默认费率1
export function editStoreAllRate(query) {
    return request({
        url: '/api/user/edit_user_store_all_rate',
        method: 'post',
        params: query
    })
}
// 修改商户默认费率2
export function editUnStoreAllRate(query) {
    return request({
        url: '/api/user/edit_user_un_store_all_rate',
        method: 'post',
        params: query
    })
}
// 修改商户默认费率3
export function editUnqrStoreAllRate(query) {
    return request({
        url: '/api/user/edit_user_unqr_store_all_rate',
        method: 'post',
        params: query
    })
}

// 获取花呗分期成本费率
export function getTokioRate(query) {
    return request({
        url: '/api/alipayopen/user_rate',
        method: 'post',
        params: query
    })
}

// 修改花呗分期成本费率
export function upUserTokioRate(query) {
    return request({
        url: '/api/alipayopen/set_user_rate',
        method: 'post',
        params: query
    })
}

// 获取代理商
export function getSubUsers(query) {
    return request({
        url: '/api/user/get_sub_users',
        method: 'get',
        params: query
    })
}

// 转移代理商
export function updateAffiliation(query) {
    return request({
        url: '/api/user/update_affiliation',
        method: 'post',
        params: query
    })
}

// 设置比例
export function addPercentage(query) {
    return request({
        url: '/api/user/addPercentage',
        method: 'post',
        params: query
    })
}

// 获取用户手机号
export function getUserPhone(query) {
    return request({
        url: '/api/user/getUserPhone',
        method: 'post',
        params: query
    })
}
// 获取代理商返现规则
export function getUserRule(query) {
    return request({
        url: '/api/user/get_user_rule',
        method: 'post',
        params: query
    })
}
// 添加代理商规则
export function addUserRule(query) {
    return request({
        url: '/api/user/add_user_rule',
        method: 'post',
        params: query
    })
}
// 查询该代理下是否有商户产生返现
export function getStoreTransaction(query) {
    return request({
        url: '/api/user/get_store_transaction',
        method: 'post',
        params: query
    })
}
// 停用
export function closeUserRule(query) {
    return request({
        url: '/api/user/close_user_rule',
        method: 'post',
        params: query
    })
}
// 下级代理返现规则
export function getCashBackRuleId(query) {
    return request({
        url: '/api/user/getCashBackRuleId',
        method: 'post',
        params: query
    })
}
// 代理商开启规则
export function openUserRule(query) {
    return request({
        url: '/api/user/openUserRule',
        method: 'post',
        params: query
    })
}
// 修改下级代理
export function editUserRule(query) {
    return request({
        url: '/api/user/editUserRule',
        method: 'post',
        params: query
    })
}
// 获取回收站的代理商
export function getDelSubUsers(query) {
    return request({
        url: '/api/user/get_del_sub_users',
        method: 'post',
        params: query
    })
}
// 彻底删除代理商
export function cdelSubUser(query) {
    return request({
        url: '/api/user/cdel_sub_user',
        method: 'post',
        params: query
    })
}
// 恢复代理商
export function fogSubUser(query) {
    return request({
        url: '/api/user/fog_sub_user',
        method: 'post',
        params: query
    })
}

// 获取代理商充值比例
export function getRateProfit(query) {
    return request({
        url: '/api/user/getRateProfit',
        method: 'post',
        params: query
    })
}
// 设置代理商充值比例
export function editRateProfit(query) {
    return request({
        url: '/api/user/editRateProfit',
        method: 'post',
        params: query
    })
}
// 获取代理零费率结算方式
export function getSettlementType(query) {
    return request({
        url: '/api/user/getSettlementType',
        method: 'post',
        params: query
    })
}
// 获取代理零费率结算方式
export function editSettlementType(query) {
    return request({
        url: '/api/user/editSettlementType',
        method: 'post',
        params: query
    })
}
// 获取代理下门店配置信息
export function userStoreSetStatus(query) {
    return request({
        url: '/api/user/user_store_set_status',
        method: 'post',
        params: query
    })
}
// 查询代理商等级
export function getAgentLevelList(query) {
    return request({
        url: '/api/user/getAgentLevelList',
        method: 'post',
        params: query
    })
}
// 新增代理商等级
export function addAgentLevel(query) {
    return request({
        url: '/api/user/addAgentLevel',
        method: 'post',
        params: query
    })
}
// 修改代理商等级
export function updateAgentLevel(query) {
    return request({
        url: '/api/user/updateAgentLevel',
        method: 'post',
        params: query
    })
}
//实名认证相关
export function getUserAuthInfo(query) {
    return request({
        url: '/api/user/getUserAuthInfo',
        method: 'post',
        params: query
    })
}
//设置代理奖励次数
export function upShoppingRewardNum(query) {
    return request({
        url: '/api/user/upShoppingRewardNum',
        method: 'post',
        params: query
    })
}
//清空代理实名认证
export function emptyUserAuth(query) {
    return request({
        url: '/api/user/emptyUserAuth',
        method: 'post',
        params: query
    })
}
//清空代理电子协议
export function emptyEsign(query) {
    return request({
        url: '/api/user/emptyEsign',
        method: 'post',
        params: query
    })
}
