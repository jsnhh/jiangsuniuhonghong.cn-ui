import request from '@/utils/request'

// 微信代金券
export function wechatCashCouponConfig(data) {
    return request({
        url: '/api/user/wechat_cash_coupon_config',
        method: 'post',
        data: data
    })
}
// 微信代金券
export function wechatMerchantCashCouponConfig(data) {
    return request({
        url: '/api/user/wechat_merchant_cash_coupon_config',
        method: 'post',
        data: data
    })
}
// 获取平台证书
export function certificates(data) {
    return request({
        url: '/api/customer/user/certificates',
        method: 'post',
        data: data
    })
}
// 设置回调地址
export function setNotifyUrl(data) {
    return request({
        url: '/api/customer/user/setNotifyUrl',
        method: 'post',
        data: data
    })
}