import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const Source = 'Source'

// var inFifteenMinutes = new Date(new Date().getTime() + 1 * 10 * 60 * 1000); // 1h
// var inFifteenMinutes = new Date(new Date().getTime() + 1 * 60 * 60 * 1000); // 1h
export function getToken() {
    return Cookies.get(TokenKey)
}

export function setToken(token) {
    // return Cookies.set(TokenKey, token, {expires: inFifteenMinutes})
    return Cookies.set(TokenKey, token)
}

export function removeToken() {
    return Cookies.remove(TokenKey)
}

export function getSource() {
    return Cookies.get(Source)
}

export function setSource(source) {
    // return Cookies.set(TokenKey, token, {expires: inFifteenMinutes})
    return Cookies.set(Source, source)
}