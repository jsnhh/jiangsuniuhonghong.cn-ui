import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: 路由配置项
 *
 * hidden: true                     // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true                 // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect             // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'               // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * query: '{"id": 1, "name": "ry"}' // 访问路由的默认传递参数
 * meta : {
    noCache: true                   // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'                  // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'                // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false               // 如果设置为false，则不会在breadcrumb面包屑中显示
    activeMenu: '/system/user'      // 当路由设置了该属性，则会高亮相对应的侧边栏。
  }
 */

export const constantRoutes = [{
        path: '/redirect',
        component: Layout,
        hidden: true,
        children: [{
            path: '/redirect/:path(.*)',
            component: (resolve) => require(['@/views/redirect'], resolve)
        }]
    },
    {
        path: '/login',
        component: (resolve) => require(['@/views/login'], resolve),
        hidden: true
    },
    {
        path: '/404',
        component: (resolve) => require(['@/views/error-page/404'], resolve),
        hidden: true
    },
    {
        path: '/401',
        component: (resolve) => require(['@/views/error-page/401'], resolve),
        hidden: true
    },
    {
        path: '/',
        component: Layout,
        redirect: '/index',
        children: [{
            path: 'index',
            component: (resolve) => require(['@/views/index'], resolve),
            name: 'Index',
            meta: { title: '首页', icon: 'dashboard', affix: true }
        }]
    },
    {
        path: '/user',
        component: Layout,
        hidden: true,
        redirect: 'noredirect',
        children: [{
            path: 'profile',
            component: (resolve) => require(['@/views/user/profile/index'], resolve),
            name: 'Profile',
            meta: { title: '个人中心', icon: 'user' }
        }]
    },
    {
        path: '/store',
        component: Layout,
        hidden: true,
        children: [{
                path: 'detail/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/storeDetail'], resolve),
                name: 'StoreDetail',
                meta: { title: '门店详细' }
            },
            {
                path: 'add_store',
                component: (resolve) => require(['@/views/store/addStore'], resolve),
                name: 'AddStore',
                meta: { title: '添加门店' }
            },
            {
                path: 'edit_store/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/editStore'], resolve),
                name: 'EditStore',
                meta: { title: '编辑门店' }
            },
            {
                path: 'electronic/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/electronicInfo'], resolve),
                name: 'ElectronicInfo',
                meta: { title: '电子协议' }
            },
            {
                path: 'furtherInfo/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/furtherStoreInfo'], resolve),
                name: 'FurtherStoreInfo',
                meta: { title: '补充资料' }
            },
            {
                path: 'lklFurtherStoreInfo/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/lklFurtherStoreInfo'], resolve),
                name: 'LklFurtherStoreInfo',
                meta: { title: '补充资料' }
            },
            {
                path: 'ysepayFurtherStoreInfo/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/ysepayFurtherStoreInfo'], resolve),
                name: 'YsepayFurtherStoreInfo',
                meta: { title: '补充资料' }
            },
            {
                path: 'ysepayElectronic/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/ysepayElectronicInfo'], resolve),
                name: 'YsepayElectronic',
                meta: { title: '电子协议' }
            },
            {
                path: 'payWays/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/payWays'], resolve),
                name: 'PayWays',
                meta: { title: '通道管理' }
            },
            {
                path: 'payWays/sort/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/payWaySort'], resolve),
                name: 'PayWaySort',
                meta: { title: '修改顺序' }
            },
            {
                path: 'qwxcode/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/qwxcode'], resolve),
                name: 'Qwxcode',
                meta: { title: '微收银插件' }
            },
            {
                path: 'wincode/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/wincode'], resolve),
                name: 'Wincode',
                meta: { title: '收钱啦插件' }
            },
            {
                path: 'cashier/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/cashier'], resolve),
                name: 'Cashier',
                meta: { title: '收银员管理' }
            },
            {
                path: 'sub_store/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/sub_store'], resolve),
                name: 'SubStore',
                meta: { title: '分店管理' }
            },
            {
                path: 'storeSeting/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/storeSeting'], resolve),
                name: 'StoreSeting',
                meta: { title: '商户功能设置' }
            },
            {
                path: 'dadaConfig/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/dadaConfig'], resolve),
                name: 'DadaConfig',
                meta: { title: '达达配送' }
            },
            {
                path: 'appletInformation/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/appletInformation'], resolve),
                name: 'AppletInformation',
                meta: { title: '完善小程序信息' }
            },
            {
                path: 'order/:storeId(\\d+)',
                component: (resolve) => require(['@/views/store/order'], resolve),
                name: 'Order',
                meta: { title: '交易流水' }
            }, {
                path: 'storeRecharge',
                component: (resolve) => require(['@/views/store/storeRecharge'], resolve),
                name: 'StoreRecharge',
                meta: { title: '商户充值设置' }
            }

        ]
    }, {
        path: '/agent',
        component: Layout,
        hidden: true,
        children: [{
                path: 'rate/rateList',
                component: (resolve) => require(['@/views/agent/rate/rateList'], resolve),
                name: 'RateList',
                meta: { title: '支付成本费率管理' }
            },
            {
                path: 'defaultRate/defaultRateList',
                component: (resolve) => require(['@/views/agent/defaultRate/defaultRateList'], resolve),
                name: 'DefaultRateList',
                meta: { title: '商户默认费率' }
            },
            {
                path: 'tokioRate/tokioRateList',
                component: (resolve) => require(['@/views/agent/tokioRate/tokioRateList'], resolve),
                name: 'TokioRateList',
                meta: { title: '花呗分期成本费率' }
            },
            {
                path: 'userAuth/:userId(\\d+)',
                component: (resolve) => require(['@/views/agent/userAuth'], resolve),
                name: 'UserAuth',
                meta: { title: '实名认证' }
            }
        ]
    },
    {
        path: '/device',
        component: Layout,
        hidden: true,
        children: [{
            path: 'index/:storeId(\\d+)',
            component: (resolve) => require(['@/views/device/index'], resolve),
            name: 'DeviceIndex',
            meta: { title: '设备列表' }
        }]
    },
    {
        path: '/commission',
        component: Layout,
        hidden: true,
        children: [{
                path: 'settlementDayList/:userId(\\d+)',
                component: (resolve) => require(['@/views/commission/settlementDayList'], resolve),
                name: 'SettlementDayList',
                meta: { title: '日分润记录' }
            },
            {
                path: 'putForwardList/:userId(\\d+)',
                component: (resolve) => require(['@/views/commission/putForwardList'], resolve),
                name: 'PutForwardList',
                meta: { title: '提现记录' }
            }
        ]
    },
    {
        path: '/alipayIot',
        component: Layout,
        hidden: true,
        children: [{
            path: 'addOperation',
            component: (resolve) => require(['@/views/alipayIot/addOperation'], resolve),
            name: 'AddOperation',
            meta: { title: 'IOT授权' }
        }, {
            path: 'bluesea',
            component: (resolve) => require(['@/views/alipayIot/bluesea'], resolve),
            name: 'Bluesea',
            meta: { title: '新蓝海报名' }
        }, {
            path: 'blueseaList',
            component: (resolve) => require(['@/views/alipayIot/blueseaList'], resolve),
            name: 'BlueseaList',
            meta: { title: '新蓝海列表' }
        }, {
            path: 'operationList',
            component: (resolve) => require(['@/views/alipayIot/operationList'], resolve),
            name: 'OperationList',
            meta: { title: '代运营授权列表' }
        }]
    },
    {
        path: '/transaction',
        component: Layout,
        hidden: true,
        children: [{
            path: 'transactionStatistics/:userId(\\d+)',
            component: (resolve) => require(['@/views/transaction/transactionStatistics'], resolve),
            name: 'TransactionStatistics',
            meta: { title: '商户交易统计' }
        }, {
            path: 'bluesea',
            component: (resolve) => require(['@/views/alipayIot/bluesea'], resolve),
            name: 'Bluesea',
            meta: { title: '新蓝海报名' }
        }]
    },
    {
        path: '/configmanager',
        component: Layout,
        hidden: true,
        children: [{
            path: 'qrCode/qrCodeBound/:cno(\\d+)',
            component: (resolve) => require(['@/views/configmanager/qrCode/qrCodeBound'], resolve),
            name: 'QrCodeBound',
            meta: { title: '码牌明细' }
        }]
    }

]

export default new Router({
    mode: 'history', // 去掉url中的#
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
})